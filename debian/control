Source: xwiimote
Section: devel
Priority: optional
Maintainer: Nobuhiro Iwamatsu <iwamatsu@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libncurses-dev,
 libudev-dev,
 pkgconf,
# prevent accidental backports after the 64-bit time_t transition
 dpkg-dev (>= 1.22.5),
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://github.com/xwiimote/xwiimote
Vcs-Git: https://salsa.debian.org/debian/xwiimote.git
Vcs-Browser: https://salsa.debian.org/debian/xwiimote

Package: xwiimote
Architecture: linux-any
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: libxwiimote2 (<< 2-2), libxwiimote1
Replaces: libxwiimote2 (<< 2-2), libxwiimote1
Description: Nintendo Wii Remote Linux Device Driver Tools
 This provides tools related to the open source Nintendo Wii
 Remote Linux device driver.
 .
 This contains the tool for displaying Wii remote connected and the setting
 tool which uses operation of Wii remote as keyboard.

Package: libxwiimote2t64
Provides: ${t64:Provides}
Replaces: libxwiimote2
Section: libs
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Breaks: libxwiimote2 (<< ${source:Version}), libxwiimote1 (<< 0.3+20120630-6)
Description: xwiimote library - runtime
 This package contains the runtime library files needed to run software
 using xwiimote.

Package: libxwiimote-dev
Section: libdevel
Architecture: linux-any
Depends: ${shlibs:Depends}, ${misc:Depends}, libxwiimote2t64 (= ${binary:Version}), libudev-dev
Breaks: libxwiimote2 (<< 2-2)
Replaces: libxwiimote2 (<< 2-2)
Multi-Arch: same
Description: xwiimote library - development
 This package contains the header and development files needed to build
 programs and packages using xwiimote.
